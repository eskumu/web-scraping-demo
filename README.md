# Homework 4 web scraping
> Kristjan Tärk

Python spider for finding all computers in ordi.ee store.

Spider is written with `scrapy` library. This can be installed with pip.

```
pip install scrapy
```

Spider finds computer name, price and picture. 
If computer has no picture, then picture href is null.

To run this spider and collect data to `data.json` file use commandline.

```
scrapy runspider ordi.py -o data.json
```
