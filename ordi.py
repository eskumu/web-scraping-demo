# -*- coding: utf-8 -*-
"""
Spider for finding all desktop computers from ordi.ee store.

@author: Kristjan Tärk
"""
import scrapy


class OrdiSpider(scrapy.Spider):
    name = "ordi_spider"
    start_urls = ['https://ordi.eu/lauaarvutid']

    def parse(self, response):
        """
        Parser method for getting all desktop computers.

        Parser finds computer name, price and image.
        """
        item_selector = '.item, .item-first, .item-last'
        for computer in response.css(item_selector):
            NAME_SELECTOR = '.product-name a ::text'
            PRICE_SELECTOR = '.price ::text'
            IMAGE_SELECTOR = 'img ::attr(src)'

            price = self._clear_price(computer.css(PRICE_SELECTOR).get())
            image_href = self._clear_img(computer.css(IMAGE_SELECTOR).get())
            yield {
                'Product name': computer.css(NAME_SELECTOR).get(),
                'Price': price,
                'Picture href': image_href,
            }
        NEXT_PAGE_SELECTOR = ".next ::attr(href)"
        next_page = response.css(NEXT_PAGE_SELECTOR).get()
        # Recursive call to find computers from next page
        if next_page:
            yield scrapy.Request(
                response.urljoin(next_page),
                callback=self.parse
            )

    @staticmethod
    def _clear_img(image_path):
        """
        Format image href.

        If image is just a placeholder, returns None.
        """
        if image_path is None:
            return None
        if image_path.endswith("/placeholder/default/small_image.jpg"):
            return None
        return image_path

    @staticmethod
    def _clear_price(price):
        """
        Formats price string to be float instead of string
        :param price: Price string
        """
        if price is None:
            return None
        _price = price.replace("€", "").replace(",", ".")
        try:
            return float(_price)
        except ValueError:
            print("Error converting price to integer, price=" + price)
            return None
